const chunkPrefix = "[name]";

let vueConfig = {
  filenameHashing: false,
  productionSourceMap: false,
  pages: {
    sm: "src/sm/main.js",
    gm: "src/gm/main.js",
  },
  chainWebpack: (config) => {
    Object.keys(vueConfig.pages).forEach(function(key) {
      config.plugins.delete("html-" + key);
      config.plugins.delete("preload-" + key);
      config.plugins.delete("prefetch-" + key);
    });
  },
  css: {
    extract: false,
  },
  configureWebpack: {
    optimization: {
      splitChunks: false,
    },
    output: {
      filename: `${chunkPrefix}.js`,
      chunkFilename: `${chunkPrefix}.js`,
    },
  },
};

module.exports = vueConfig;
