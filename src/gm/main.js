import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

let id = "gm";
var div = document.createElement("div");
div.id = id;
document.body.appendChild(div);

window.GmWidget = {
  init(options) {
    new Vue({
      render: (h) =>
        h("app", {
          props: {
            ...options,
          },
        }),
      components: {
        App,
      },
    }).$mount("#" + id);
  },
};
